# Artifact Uprising Code Challenge

## How to deploy
There's an included docker-compose yaml file that only requires you have installed Docker with compose, and an internet connection.

`docker-compose build --no-cache --pull`

Standard docker-compose build is sufficient, if you've been using node/elixir/nginx docker images
recently, a --no-cache --pull might be warranted. I've tested this against the latest release
of any images I didn't lock versions on.

`docker-compose up`

This will launch all the containers in the compose file, and cat their log files to the terminal.
It will take over the current console session, so make a new tab first. Pressing Ctrl+C will terminate the containers.

You should be able to access the site at http://localhost:4567/

## Design Decisions

There is a single back-end service, a single front-end application, and an edge service.

If there were more time or this was going to be deployed for real, the back-end should have
either been 1) split into two services or 2) renamed to denote that it no longer is just for the cart.

The back-end service is written in Elixir with Plug and Cowboy. Yes it's eclectic.
It's not the fastest single-threaded languages ever, but it handles concurrency better than any
other language I've used, and provides very predictable CPU and memory utilization.

The front-end is written in JavaScript with Vue and Bulma. Vue is a very straight-forward framework.
It has few opinions, and doesn't get in the way. Bulma is a CSS framework that has no JS.
You need to bring your own JS with Bulma, which I think makes cleaner, more readable code.

The persistence store is PostgreSQL. It's not the most exciting thing ever,
and is totally overkill for the two tables I ended up creating.
The back-end will automatically create the tables and populate one of them with sample data on startup.
In a more serious setting I'd suggest looking at persistence that more closely aligns with the data
we're looking to work with long-term.

There's a proxy edge service running nginx that forwards to the appropriate back-end/front-end services.
I run a separate proxy edge service instead of lumping the front-end into that service because it reduces
the attackable surface of the application, and it results in easier development/debugging
(ex. Swapping the front-end service out with a webpack-dev-server is the only way this could get done in such a short time without messing with CORS)

## Tests

Tests for the front and backend are run on build.

Given how much of this is CRUD, there's a not a whole lot to test.
I stretched what I would normally test to give you more to look at.

## What's next?

I've capped myself at one day working on this.
Next thing I'd do is probably add in authentication so that we can support multiple users.
You'll see in the back-end that the database is already ready for that. I've also pre-stubbed a
plug filter that would check for authentication and determine the user's id (though that's currently hard-coded).

I don't think I can get that added in with the time remaining today.
