import axios from "axios";

const server = axios.create({
  baseURL: "/api"
});

export default {
  install(Vue, options) {
    Vue.mixin({
      computed: {
        $server() {
          return server;
        }
      }
    });
  }
};
