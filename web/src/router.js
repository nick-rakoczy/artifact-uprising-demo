import Vue from "vue";
import Router from "vue-router";

import Home from "@/home/Home.vue";
import Cart from "@/cart/Cart.vue";

Vue.use(Router);

export default new Router({
  base: "/",
  mode: "history",
  routes: [
    {path: "/", name: "home", component: Home},
    {path: "/cart", name: "cart", component: Cart}
  ]
});
