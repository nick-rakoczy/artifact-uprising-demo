import "./app.scss";
import "bulma/bulma.sass";

import Vue from "vue";
import router from "./router.js";

import App from "./App.vue";
import server from "./server.js";

Vue.use(server);

const vueConfig = {
  router,
  render: (h) => h(App)
};

new Vue(vueConfig).$mount("body");
