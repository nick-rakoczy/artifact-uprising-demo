const path = require("path");
const resolve = (dir) => path.join(__dirname, "..", dir);

const {VueLoaderPlugin: VueLoader} = require("vue-loader");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const StylelintWebpackPlugin = require("stylelint-webpack-plugin");

module.exports = {
  mode: "production",
  entry: {
    app: [resolve("src/main.js")]
  },
  devtool: "source-map",
  resolve: {
    extensions: [".js", ".vue", ".json", ".css", ".scss", ".sass"],
    alias: {
      "@": resolve("src")
    }
  },
  plugins: [
    new VueLoader(),
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: "./public/index.html",
      inject: true
    }),
    new CopyWebpackPlugin([{
      from: resolve("public"),
      to: "./"
    }]),
    new StylelintWebpackPlugin()
  ],
  module: {
    rules: [
      {
        use: "eslint-loader",
        test: /\.(js|jsx)$/,
        enforce: "pre",
        exclude: /node_modules/
      },
      {
        use: "vue-loader",
        test: /\.vue$/,
        exclude: /node_modules/
      },
      {
        use: [
          {loader: "style-loader"},
          {loader: "css-loader", options: {sourceMap: true}},
          {loader: "postcss-loader", options: {sourceMap: true}},
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
              implementation: require("sass")
            }
          }
        ],
        test: /\.(scss|sass)$/
      }
    ]
  }
};
