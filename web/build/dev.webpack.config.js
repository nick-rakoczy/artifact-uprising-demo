module.exports = {
  ...require("./webpack.config.js"),
  mode: "development",
  devServer: {
    publicPath: "/",
    port: 8081,
    host: "0.0.0.0",
    disableHostCheck: true,
    hot: false,
    inline: false,
    historyApiFallback: {
      rewrites: [
        {from: /.*/, to: `${process.env.BASE_URL}/index.html`}
      ]
    }
  },
  watchOptions: {
    poll: 1000,
    ignored: /node_modules/
  }
}
