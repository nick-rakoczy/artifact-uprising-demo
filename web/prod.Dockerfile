FROM node:latest AS build
WORKDIR /app
COPY . /app
RUN npm ci
RUN npm test
RUN npm run build

FROM nginx
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/dist /usr/share/nginx/html
