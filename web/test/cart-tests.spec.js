import assert from "assert";

import Vue from "vue";
import Cart from "@/cart/Cart.vue";

const cart = new Vue({
  ...Cart,
  created() {} // Disable created to prevent unhandled promise log spam
});

describe("Cart Component", () => {
  it("Item Count is Sum", () => {
    const qtys = [1, 2, 3];
    const items = qtys.map((qty) => ({qty}));
    cart.$data.items = items;
    assert.equal(cart.itemCount, 6);
  });

  it("Line Total is Sum", () => {
    assert.equal(cart.$options.filters.lineTotal({qty: 1, price: 10.5}), "10.50");
    assert.equal(cart.$options.filters.lineTotal({qty: 2, price: 12.2}), "24.40");
    assert.equal(cart.$options.filters.lineTotal({qty: 3, price: 0.91}), "2.73");
  });
});
