const prod = process.env.NODE_ENV === "production";
const warnThenError = prod ? "error" : "warn";

module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 2019,
    sourceType: "module",
    allowImportExportEverywhere: true
  },
  parser: "vue-eslint-parser",
  env: {
    amd: true,
    browser: true,
    es6: true
  },
  plugins: [
    "vue"
  ],
  settings: {
    "import/resolver": {
      webpack: {
        config: "build/webpack.config.js"
      }
    }
  },
  rules: {
    "arrow-parens": [2, "always"],
    "comma-dangle": ["error", "never"],
    "max-len": [2, 120, 2, {
      "ignoreUrls": true,
      "ignoreComments": false
    }],
    "no-debugger": warnThenError,
    "no-console": [warnThenError, {"allow": ["error"]}],
    "space-in-parens": ["error", "never"],
    "quotes": ["error", "double", "avoid-escape"],
    "prefer-object-spread": ["warn"],

    "vue/component-name-in-template-casing": ["error", "kebab-case"],
    "vue/html-indent": ["error", 2, {
      "attribute": 1,
      "closeBracket": 0,
      "alignAttributesVertically": true,
      "ignores": []
    }],
    "vue/html-closing-bracket-newline": ["error", {
      "singleline": "never",
      "multiline": "never"
    }],
    "vue/singleline-html-element-content-newline": "off",
    "vue/max-attributes-per-line": [2, {
      "singleline": 99,
      "multiline": {
        "max": 1,
        "allowFirstLine": true
      }
    }]
  }
};
