defmodule Cart do

  @query """
    CREATE TABLE IF NOT EXISTS cart_items (
      user_id varchar(36) not null,
      item_id varchar(36) not null,
      qty int not null,
      primary key (user_id, item_id)
    )
  """
  def init_db() do
    Postgrex.query!(Postgrex, @query, [])
    |> IO.inspect(label: "Cart.init_db")
  end

  @query """
    SELECT *
    FROM cart_items
    WHERE user_id = $1
  """
  def get(user_id, dbo \\ Postgrex) do
    dbo.query!(dbo, @query, [user_id])
  end

  @update_query """
    UPDATE cart_items
    SET qty = $3
    WHERE (user_id, item_id) = ($1, $2)
  """
  @insert_query """
    INSERT INTO cart_items (user_id, item_id, qty)
    VALUES ($1, $2, $3)
  """
  def update_qty(user_id, item_id, qty, dbo \\ Postgrex) do
    query =
      case already_in_cart?(user_id, item_id, dbo) do
        true -> @update_query
        false -> @insert_query
      end

    dbo.query!(dbo, query, [user_id, item_id, qty])
  end

  @query """
    SELECT 'x'
    FROM cart_items
    WHERE (user_id, item_id) = ($1, $2)
  """
  def already_in_cart?(user_id, item_id, dbo \\ Postgrex) do
    results = dbo.query!(dbo, @query, [user_id, item_id])
    not match?([], results.rows)
  end

  @query """
    DELETE FROM cart_items
    WHERE (user_id, item_id) = ($1, $2)
  """
  def remove_item(user_id, item_id, dbo \\ Postgrex) do
    dbo.query!(dbo, @query, [user_id, item_id])
  end

  @query """
    DELETE FROM cart_items
    WHERE user_id = $1
  """
  def clear(user_id, dbo \\ Postgrex) do
    dbo.query!(dbo, @query, [user_id])
  end
end
