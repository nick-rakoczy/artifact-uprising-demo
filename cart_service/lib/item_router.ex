defmodule ItemRouter do
  use Plug.Router

  plug :match
  plug :dispatch

  get "/" do
    items = Items.get_all()
    payload = Poison.encode!(items)

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, payload)
  end
end
