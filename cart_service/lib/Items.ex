defmodule Items do
  @query """
    CREATE TABLE IF NOT EXISTS items (
      item_id varchar(36),
      name varchar(255),
      price int,
      primary key (item_id)
    )
  """
  def init_db(dbo \\ Postgrex) do
    dbo.query!(dbo, @query, [])

    if empty?() do
      init_sample_items()
      |> IO.inspect(label: "Items.init_db")
    end
  end

  @query """
    SELECT COUNT(*)
    FROM items
  """
  def empty?(dbo \\ Postgrex) do
    result =
      dbo.query!(dbo, @query, [])
      |> IO.inspect(label: "empty?")

    match?([[0]], result.rows)
  end

  def init_sample_items(dbo \\ Postgrex) do
    priv_dir =
      :cart_service
      |> :code.priv_dir()
      |> to_string()
    sample_file = priv_dir <> "/MOCK_DATA.json"

    {:ok, sample_file_json} = File.read(sample_file)
    {:ok, sample_data} = Poison.decode(sample_file_json)

    sample_data
    |> Stream.map(&[&1["name"], &1["price"]])
    |> Enum.each(&apply(__MODULE__, :create_item, &1))
  end

  @query """
    INSERT INTO items (item_id, name, price)
    VALUES ($1, $2, $3)
  """
  def create_item(name, price, dbo \\ Postgrex) do
    item_id = UUID.uuid4()
    dbo.query!(dbo, @query, [item_id, name, price])
  end

  @query """
    SELECT item_id, name, price
    FROM items
    ORDER BY name ASC
  """
  def get_all(dbo \\ Postgrex) do
    result = dbo.query!(dbo, @query, [])

    result.rows
    |> Enum.map(fn [item_id, name, price] ->
      %{
        item_id: item_id, name: name, price: price / 100
      }
    end)
  end
end
