defmodule CartRouter do
  use Plug.Router

  plug Plug.Parsers,
    parsers: [:urlencoded, :json],
    json_decoder: Poison

  plug :match
  plug :dispatch

  forward("/api/cart", to: UserCartRouter)
  forward("/api/items", to: ItemRouter)

  match _ do
    send_resp(conn, 404, "Not Found")
  end
end
