defmodule CartService do
  require Logger

  use Application

  def supervisor_children do
    []
  end

  def supervisor_config do
    [
      strategy: :rest_for_one,
      name: __MODULE__.Supervisor
    ]
  end

  cond do
    Mix.env() === :test ->
      def start(_, _) do
        Supervisor.start_link([], supervisor_config())
      end

    true ->
      def start(_, _) do
        children = [
          __MODULE__.supervisor_children(),
          postgrex_child_spec(),
          cowboy_listener_child_spec()
        ]

        children
        |> List.flatten()
        |> Supervisor.start_link(supervisor_config())
      after
        Cart.init_db()
        Items.init_db()
      end
  end

  defp cowboy_listener_child_spec do
    Plug.Cowboy.child_spec(scheme: :http, plug: CartRouter, options: [port: 8080])
  end

  defp postgrex_child_spec do
    Postgrex.child_spec(name: Postgrex)
  end
end
