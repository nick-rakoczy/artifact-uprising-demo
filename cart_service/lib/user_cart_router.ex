defmodule UserCartRouter do
  use Plug.Router

  plug :match
  plug :extract_user_id
  plug :dispatch

  get "/" do
    items = Items.get_all()

    cart =
      conn.private.user_id
      |> Cart.get()
      |> extract_rows_from_result()
      |> Enum.map(&populate_item_data(&1, items))

    json =
      %{
        cart: cart,
        total: get_cart_total(cart)
      }
      |> Poison.encode!()

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, json)
  end

  post "/" do
    user_id = conn.private.user_id

    case Map.get(conn.body_params, "item_id", nil) do
      nil ->
        send_resp(conn, 400, "Invalid request, missing item_id")

      item_id ->
        qty =
          user_id
          |> Cart.get()
          |> extract_rows_from_result()
          |> get_next_increment(item_id)

        get_next_increment(user_id, item_id)
        Cart.update_qty(user_id, item_id, qty)
        send_resp(conn, 200, "OK")
    end
  end

  put "/" do
    user_id = conn.private.user_id

    cart_items =
      conn.body_params
      |> Map.get("cart", [])

    for %{"item_id" => item_id, "qty" => qty} <- cart_items do
      case qty do
        0 -> Cart.remove_item(user_id, item_id)
        q -> Cart.update_qty(user_id, item_id, q)
      end
    end
    |> IO.inspect(label: "update-query")

    send_resp(conn, 200, "OK")
  end

  delete "/" do
    conn.private.user_id
    |> Cart.clear()

    send_resp(conn, 200, "OK")
  end

  @doc """
    Calculate the next quantity value for a given item_id.

    iex> cart = [%{item_id: 0, qty: 1}]
    iex> get_next_increment(cart, 1)
    1
    iex> get_next_increment(cart, 0)
    2
  """
  def get_next_increment(cart, item_id) do
    cart
    |> Enum.find(&(item_id == &1.item_id))
    |> case do
      nil -> 1
      %{qty: qty} -> qty + 1
    end
  end

  @doc """
    Calculate the total price of the cart.

    iex> [%{price: 10.5, qty: 1}, %{price: 12.2, qty: 2}, %{price: 0.91, qty: 3}]
    ...> |> get_cart_total()
    ...> |> Float.round(2)
    37.63
  """
  def get_cart_total(cart) do
    cart
    |> Stream.map(fn %{price: price, qty: qty} -> (price || 0) * (qty || 0) end)
    |> Enum.sum()
  end

  @doc """
    Populate the Cart item with the appropriate item data.

    iex> items = [%{item_id: 0, name: "Bolt", price: 10.50}]
    iex> cart_item = %{item_id: 0, qty: 12}
    iex> populate_item_data(cart_item, items)
    %{item_id: 0, name: "Bolt", price: 10.5, qty: 12}
    iex> r = populate_item_data(%{cart_item | item_id: 1}, items)
    iex> match?(%{item_id: 1, name: nil, price: nil}, r)
    true
  """
  def populate_item_data(cart_item, items \\ Items.get_all()) do
    {name, price} =
      case Enum.find(items, fn %{item_id: item_id} -> item_id === cart_item.item_id end) do
        nil -> {nil, nil}
        item -> {item.name, item.price}
      end

    cart_item
    |> Map.put(:name, name)
    |> Map.put(:price, price)
  end

  @doc """
    Extract a cart record from a result set

    iex> rows = [[nil, 0, 1], [nil, 1, 2], [nil, 2, 3]]
    iex> result = %{rows: rows}
    iex> extract_rows_from_result(result)
    [%{item_id: 0, qty: 1}, %{item_id: 1, qty: 2}, %{item_id: 2, qty: 3}]
  """
  def extract_rows_from_result(result) do
    for [_user_id, item_id, qty] <- result.rows do
      %{
        item_id: item_id,
        qty: qty
      }
    end
  end

  def extract_user_id(conn, _opts) do
    user_id = "00000000-0000-0000-0000-000000000000"

    # TODO: Pull the user_id from a bearer token here.

    conn
    |> put_private(:user_id, user_id)
  end
end
