defmodule CartService.MixProject do
  use Mix.Project

  def project do
    [
      app: :cart_service,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: releases()
    ]
  end

  def releases do
    [
      cart_service: [
        include_executables_for: [:unix],
        application: [cart_service: :permanent]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {CartService, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:plug, "~> 1.10"},
      {:plug_cowboy, "~> 2.1"},
      {:poison, "~> 4.0"},
      {:postgrex, "~> 0.15.3"},
      {:uuid, "~> 1.1"},
      {:remix, "~> 0.0.2", only: :dev}
    ]
  end
end
