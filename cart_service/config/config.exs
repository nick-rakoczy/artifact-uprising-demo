use Mix.Config

import_config("global/*.exs")
import_config("#{Mix.env()}/*.exs")
